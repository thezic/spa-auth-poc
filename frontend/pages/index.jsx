import fetch from 'isomorphic-unfetch'
import {useState, useEffect} from 'react'

const API_ROOT = 'http://127.0.0.1:4000'

/*
 * fetchData
 * Fetch data from API server.
 */
const fetchData = async (token) => {
  const data = await fetch(`${API_ROOT}/data`, {
    headers: {
      Authorization: `token ${token}`,
    }
  })
  return data.json()
}

/*
 * login
 * create session on local server
 */
const login = async () => {
  const res = await fetch(`/api/login`, {
    credentials: 'same-origin',
  })
  const { token } = await res.json()
  return token;
}

/*
 * logout
 * destroy session on local server
 */
const logout = async () => {
    const res = fetch(`/api/logout`, {
      credentials: 'same-origin',
    })
}

/*
 * fetchTokenFromSession
 * Fetch token from local server, if session exists
 */
const fetchTokenFromSession = async () => {

  const res = await fetch(`/api/fetch-token`, {
    credentials: 'include',
  })

  const { token } = await res.json()
  return token
}

const  Index = () => {
  const [data, setData] = useState(null)
  const [error, setError] = useState(null)
  const [token, setToken] = useState(null)

  const handlFetchClick = () => fetchData(token).then(setData).catch(setError)
  const handleLoginClick = () => login().then(setToken).catch(setError)
  const handleLogoutClick = () => logout().then(() => setToken(null)).catch(setError)

  // Fetch token if we already have a session
  useEffect(() => {
    fetchTokenFromSession().then(setToken).catch(setError)
  }, [])

  return (
    <div>
      <div>
        { !token && <button onClick={handleLoginClick}>login</button> }
        { token && <button onClick={handleLogoutClick}>logout</button>}
      </div>
      <div>
        <button onClick={handlFetchClick}>Fetch data</button>
      </div>
      <div>Last response: {JSON.stringify(data)}</div>
      { error && <div style="color: red;">{error}</div>}
    </div>
  )
}

export default Index