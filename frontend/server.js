const express = require('express')
const session = require('express-session')
const next = require('next')

const fetch = require('isomorphic-unfetch')

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

const API_SERVER = 'http://127.0.0.1:4000'


const fetchToken = async () => {
  const data = await fetch(API_SERVER + '/get-token')
  return data.json()
}


app
  .prepare()
  .then(() => {
    const server = express()

    server.use(session({ secret: 'my-secret'}))

    server.get('/api/login', async (req, res) => {
      const {token} = await fetchToken()
      req.session.token = token
      console.log(req.session.token)
      return res.json({token})
    })

    server.get('/api/fetch-token', async (req, res) => {
      const token = req.session.token
      console.log(req.session.token)
      return res.json({token})
    })

    server.get('/api/logout', (req, res, next) => {
      req.session.destroy()
      return res.json({ status: 'ok' })
    })

    server.get('*', (req, res) => {
      return handle(req, res)
    })

    server.listen(3000, err => {
      if (err) throw err
      console.log('> Ready on http://localhost:3000')
    })
  })
  .catch(ex => {
    console.error(ex.stack)
    process.exit(1)
  })