const express = require('express')
const crypto = require('crypto')

// Storage for tokens
let token_list = []

const app = express()

const FORBIDDEN = 'forbidden'

// generateToken
//   Creates a new token and returns token
const generateToken = () => {
  const salt = Math.random()
  const now = new Date()
  const hash = crypto.createHash('sha256')

  hash.write('' + now.getTime() + salt)
  const digest = hash.digest('hex')

  return digest
}

// verifyToken
//   Express middleware, verifying a token is supplied and valid
const verifyToken = (req, res, next) => {
  console.log('Verifying token')
  const auth = req.header('Authorization')
  if(!auth)
    return next(FORBIDDEN)

  const matches = auth.match(/token (\w+)/)
  if(!matches)
    return next(FORBIDDEN)

  const [, token] = matches

  if(!token_list.includes(token)) {
    return next(FORBIDDEN)
  }

  console.log(token)
  return next()
}


app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
  next()
})


// getToken
//   Express endpoint, sending a new token back tu client
app.get('/get-token', (req, res) => {
  let token = generateToken()
  token_list.push(token)
  res.json({ token })
})


app.get('/data', verifyToken, (req, res) => {
  res.json({
    data: 'This is secured data'
  })
})


// Error handling
app.use((err, req, res, next) => {
  if(res.headersSent) {
    return next(err)
  }

  if(err === FORBIDDEN) {
    res.status(403)
    return res.json({
      error: 'Not authorized'
    })
  }

  return next(err)
})


app.listen(4000, () => {
  console.log('API Server listening on port 4000')
})
